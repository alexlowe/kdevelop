# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Franklin Weng <franklin@goodhorse.idv.tw>, 2010, 2011, 2012, 2013.
# pan93412 <pan93412@gmail.com>, 2018.
#
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kdevmakebuilder\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2018-12-02 21:19+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: makebuilderpreferences.cpp:70
#, fuzzy, kde-format
#| msgid "Make"
msgctxt "@title:tab"
msgid "Make"
msgstr "Make"

#: makebuilderpreferences.cpp:75
#, fuzzy, kde-format
#| msgid "Configure Make settings"
msgctxt "@title:tab"
msgid "Configure Make Settings"
msgstr "設定 Make 設定"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: makeconfig.ui:32
#, fuzzy, kde-format
#| msgid "&Abort on first error:"
msgctxt "@option:check"
msgid "&Abort on first error:"
msgstr "遇到錯誤就中止(&A)："

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: makeconfig.ui:52
#, fuzzy, kde-format
#| msgid "&Display commands but do not execute them:"
msgctxt "@option:check"
msgid "&Display commands but do not execute them:"
msgstr "顯示指令但不執行(&D)："

#. i18n: ectx: property (text), widget (QLabel, label_31)
#: makeconfig.ui:72
#, fuzzy, kde-format
#| msgid "Insta&ll as root:"
msgctxt "@option:check"
msgid "Insta&ll as root:"
msgstr "以 root 安裝(&L)："

#. i18n: ectx: property (text), widget (QLabel, rootinstallationcommandLabel)
#: makeconfig.ui:95
#, fuzzy, kde-format
#| msgid "Root installation &command:"
msgctxt "@label:listbox"
msgid "Root installation &command:"
msgstr "Root 安裝指令(&C)："

#. i18n: ectx: property (text), widget (QLabel, label)
#: makeconfig.ui:133
#, fuzzy, kde-format
#| msgid "&Number of simultaneous jobs:"
msgctxt "@label:spinbox"
msgid "&Number of simultaneous jobs:"
msgstr "同時工作的數量(&N)："

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: makeconfig.ui:156
#, fuzzy, kde-format
#| msgid "&Make executable:"
msgctxt "@label:chooser"
msgid "&Make executable:"
msgstr "Make 執行檔(&M)："

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: makeconfig.ui:179
#, fuzzy, kde-format
#| msgid "Default make &target:"
msgctxt "@label:textbox"
msgid "Default make &target:"
msgstr "預設的 Make 目標(&T)："

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: makeconfig.ui:199
#, fuzzy, kde-format
#| msgid "Additional ma&ke options:"
msgctxt "@label:textbox"
msgid "Additional ma&ke options:"
msgstr "Make 的額外選項(&K)："

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: makeconfig.ui:219
#, fuzzy, kde-format
#| msgid "Active environment &profile:"
msgctxt "@label:chooser"
msgid "Active environment &profile:"
msgstr "作用的環境設定檔(&P)："

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: makeconfig.ui:263
#, fuzzy, kde-format
#| msgid "Override number of &jobs:"
msgctxt "@option:check"
msgid "Override number of &jobs:"
msgstr "覆蓋工作數量(&J)："

#: makejob.cpp:80
#, kde-format
msgid "Make (%1): %2"
msgstr "Make（%1）：%2"

#: makejob.cpp:82
#, kde-format
msgid "Make (%1)"
msgstr "Make (%1)"

#: makejob.cpp:84
#, kde-format
msgid "Make"
msgstr "Make"

#: makejob.cpp:98
#, kde-format
msgid "Build item no longer available"
msgstr "已無法建立項目"

#: makejob.cpp:105
#, kde-format
msgid "Internal error: cannot build a file item"
msgstr "內部錯誤：無法建立檔案項目"

#~ msgid "kdesu"
#~ msgstr "kdesu"

#~ msgid "kdesudo"
#~ msgstr "kdesudo"

#~ msgid "gksu"
#~ msgstr "gksu"
